// 必须对container-widget下的组件进行全局注册，否则会造成GridColWidgetDesign中找不到GridWidgetDesign组件的问题，可能是VUE中父子嵌套引入的BUG

import Vue from 'vue'

const requireComponent = require.context('./', false, /\w+\.vue$/)

requireComponent.keys().map(fileName => {
  let comp = requireComponent(fileName).default;
  Vue.component(comp.name, comp)
})
