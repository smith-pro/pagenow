const codeInsight = [
  // 事件交互
  {
    key: "EventBus.send",
    text: 'EventBus.send("","")',
    displayText: "//发送总线事件\nEventBus.send('','')"
  },
  {
    key: "EventBus.receive",
    text: 'EventBus.receive([""], (params) => {})',
    displayText: "//接收总线事件\nEventBus.receive([''], (params) => {})"
  },
  // 组件实例函数
  {
    key: "this.redrawComp",
    text: "this.redrawComp()",
    displayText: "//重绘组件\n//注：当通过脚本修改了组件的数据源结果集后请对组件进行重绘\nthis.redrawComp()"
  },
  {
    key: "this.findCompVmById",
    text: "this.findCompVmById('')",
    displayText: "//根据组件ID获取当前页面下的组件Vue实例\nthis.findCompVmById('')"
  },
  // 组件数据源属性
  {
    key: "this.component.compConfigData",
    text: "this.component.compConfigData",
    displayText: "//根配置属性，此变量下存储组件所有的配置属性\nthis.component.compConfigData"
  },
  {
    key: "this.component.compConfigData.ds_apiPath",
    text: "this.component.compConfigData.ds_apiPath",
    displayText: "//组件绑定的API数据源接口地址\nthis.component.compConfigData.ds_apiPath"
  },
  {
    key: "this.component.compConfigData.ds_apiHeaders",
    text: "this.component.compConfigData.ds_apiHeaders",
    displayText: "//组件绑定的API数据源的Headers数据\nthis.component.compConfigData.ds_apiHeaders"
  },
  {
    key: "this.component.compConfigData.ds_apiPostData",
    text: "this.component.compConfigData.ds_apiPostData",
    displayText: "//组件绑定的API数据源的POST参数对象\nthis.component.compConfigData.ds_apiPostData"
  },
  {
    key: "this.component.compConfigData.ds_sql",
    text: "this.component.compConfigData.ds_sql",
    displayText: "//组件绑定的SQL查询语句\nthis.component.compConfigData.ds_sql"
  },
  {
    key: "this.component.compConfigData.ds_csvDatasourceName",
    text: "this.component.compConfigData.ds_csvDatasourceName",
    displayText: "//组件绑定的CSV数据源名称\nthis.component.compConfigData.ds_csvDatasourceName"
  },
  {
    key: "this.component.compConfigData.ds_resultObj",
    text: "this.component.compConfigData.ds_resultObj",
    displayText: "//组件绑定的数据源结果集对象\nthis.component.compConfigData.ds_resultObj"
  },
  // PnUtil
  {
    key: "PnUtil.uuid",
    text: "PnUtil.uuid()",
    displayText: "//生成UUID\nPnUtil.uuid()"
  },
  {
    key: "PnUtil.getTimestamp",
    text: "PnUtil.getTimestamp()",
    displayText: "//获取当前时间戳\nPnUtil.getTimestamp()"
  },
  {
    key: "PnUtil.getRootPath",
    text: "PnUtil.getRootPath()",
    displayText: "//获取项目工程根路径\nPnUtil.getRootPath()"
  },
  {
    key: "PnUtil.jumpLink",
    text: 'PnUtil.jumpLink("","_blank")',
    displayText: "//跳转链接\n//第一参数：链接地址；第二参数：页面打开方式（_self，_blank）\nPnUtil.jumpLink('href','_self')"
  },
  {
    key: "PnUtil.openIframeModal",
    text: 'PnUtil.openIframeModal("","100%","100%",true)',
    displayText: "//打开一个模态窗\n//第一参数：链接地址\n//第二参数：窗口宽度，字符串类型\n//第三参数：窗口高度，字符串类型\n//第四参数：是否显示滚动条，布尔类型，默认auto\nPnUtil.openIframeModal('href','100%','100%',true)"
  },
  {
    key: "PnUtil.deepClone",
    text: "PnUtil.deepClone(obj)",
    displayText: "//深拷贝对象\n//第一参数：被拷贝对象\nPnUtil.deepClone(obj)"
  },
  {
    key: "PnUtil.addUrlParams",
    text: 'PnUtil.addUrlParams("","")',
    displayText: "//动态添加URL参数\n//第一参数：键值；第二参数：数值\nPnUtil.addUrlParams('key','value')"
  },
  {
    key: "PnUtil.deleteUrlParams",
    text: 'PnUtil.deleteUrlParams("")',
    displayText: "//动态删除URL参数\n//第一参数：参数键值名称\nPnUtil.deleteUrlParams('key')"
  },
  {
    key: "PnUtil.getUrlParam",
    text: 'PnUtil.getUrlParam("")',
    displayText: "//获取URL参数值\n//第一参数：参数键值名称\nPnUtil.getUrlParam('key')"
  },
  {
    key: "PnUtil.deepMerge",
    text: 'PnUtil.deepMerge(obj1, obj2)',
    displayText: "//深合并对象\n//第一参数：对象1；第二参数：对象2\nPnUtil.deepMerge(obj1,obj2)"
  },
  {
    key: "PnUtil.manualLaunchInteractionStream",
    text: 'PnUtil.manualLaunchInteractionStream("","")',
    displayText: "//手动发起内置一次内置交互流\n//第一参数：发起的交互变量；第二参数：发送值\n//注意：本函数不可在组件的初始化运行脚本中立即执行\nPnUtil.manualLaunchInteractionStream('bindingVariateName', 'value')"
  },
  {
    key: "PnUtil.multiManualLaunchInteractionStream",
    text: 'PnUtil.multiManualLaunchInteractionStream([\n  {bindingVariateName: "", value: ""}\n])',
    displayText: "//手动发起内置一组内置交互流\n//参数为一个数组，其中包含多个数据对象表示每一次交互流的相关数据：\n//bindingVariateName：发起的交互变量\n//value：发送值\n//注意：本函数不可在组件的初始化运行脚本中立即执行\nPnUtil.multiManualLaunchInteractionStream([{bindingVariateName: '', value: ''}])"
  },
  // Http请求函数
  {
    key: "PnApi.httpQuery",
    text: 'PnApi.httpQuery("GET", "", {}, {}).then(result => {})',
    displayText: "//客户端HTTP请求\n//第一参数：请求方式（GET、POST）\n//第二参数：请求地址\n//第三参数：头信息，标准JSON对象\n//第四参数：POST请求提交数据，标准JSON对象\nPnApi.httpQuery('GET', 'apiPath', {}, {})"
  },
  {
    key: "PnApi.HttpProxyApi.httpQuery",
    text: 'PnApi.HttpProxyApi.httpQuery("GET", "", {}, {}).then(result => {})',
    displayText: "//服务器代理HTTP请求\n//第一参数：请求方式（GET、POST）\n//第二参数：请求地址\n//第三参数：头信息，标准JSON对象\n//第四参数：POST请求提交数据，标准JSON对象\nPnApi.HttpProxyApi.httpQuery('GET', 'apiPath', {}, {})"
  },
  {
    key: "PnApi.HttpProxyApi.httpQuerySync",
    text: 'PnApi.HttpProxyApi.httpQuerySync("GET", "", {}, {}).then(result => {})',
    displayText: "//服务器代理HTTP请求（同步请求）\n//注意：返回的数据如果是JSON字符串，请使用JSON.parse()进行转换\n//第一参数：请求方式（GET、POST）\n//第二参数：请求地址\n//第三参数：头信息，标准JSON对象\n//第四参数：POST请求提交数据，标准JSON对象\nPnApi.HttpProxyApi.httpQuerySync('GET', 'apiPath', {}, {})"
  },
  // 全局变量
  {
    key: "BASE_PATH",
    text: "BASE_PATH",
    displayText: "//项目工程根路径，读取形式：http://xxx/xxx\nBASE_PATH"
  },
  {
    key: "Echarts",
    text: "Echarts",
    displayText: "//全局Echarts变量\nEcharts"
  }
];

export default {
  codeInsight
}
