import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/index'

import './i18n/index'

import AsyncComputed from 'vue-async-computed'

// iView
import iView from 'view-design';
// import 'iview/dist/styles/iview.css';
// import './assets/theme/black_ui/iview.css';
import 'view-design/dist/styles/iview.css';
import './assets/theme/black_ui/iview.dark.css';

import 'animate.css'

// JQuery
import $ from 'jquery'
require('webpack-jquery-ui/draggable');
require('webpack-jquery-ui/droppable');
require('./utils/webpack-jquery-ui-selectable');
require('webpack-jquery-ui/css');

// 引入Echart5.x组件
import * as echarts from 'echarts';
import 'echarts-gl'

// 引入UEditor组件
import VueUeditorWrap from 'vue-ueditor-wrap'

// 引入图片预览组件
import 'viewerjs/dist/viewer.css'
import Viewer from 'v-viewer'

// import vue-codemirror
import VueCodemirror from 'vue-codemirror'
// import codemirror base style
import 'codemirror/lib/codemirror.css'
// import codemirror theme style
import './assets/codemirror/dark-theme.css'
// import codemirror fullscreen style
import './assets/codemirror/fullscreen.css'
// import codemirror addon about js
import 'codemirror/addon/edit/closebrackets'
import 'codemirror/addon/display/placeholder'
import 'codemirror/addon/display/autorefresh'
// import codemirror lint
import './assets/codemirror/lint.css'
import 'codemirror/addon/lint/lint'
import 'codemirror/addon/lint/javascript-lint'
import 'codemirror/addon/lint/json-lint'
import 'codemirror/addon/lint/html-lint'
// import codemirror language js
import 'codemirror/mode/javascript/javascript.js'
import 'codemirror/mode/sql/sql'
import 'codemirror/mode/css/css'
import 'codemirror/mode/htmlmixed/htmlmixed'
// import codemirror hint
import 'codemirror/addon/hint/show-hint';
import './assets/codemirror/show-hint.css';
import 'codemirror/addon/hint/sql-hint';
import 'codemirror/addon/hint/javascript-hint';
import 'codemirror/addon/hint/css-hint';
import 'codemirror/addon/hint/html-hint';
import 'codemirror/addon/hint/anyword-hint';
// import codemirror keyMap
import 'codemirror/mode/clike/clike.js'
import 'codemirror/addon/edit/matchbrackets.js'
import 'codemirror/addon/comment/comment.js'
import 'codemirror/addon/dialog/dialog.js'
import 'codemirror/addon/dialog/dialog.css'
import 'codemirror/addon/search/searchcursor.js'
import 'codemirror/addon/search/search.js'
import 'codemirror/keymap/sublime.js'

import jshint from 'jshint'
require('script-loader!jsonlint')
import HTMLHint from 'htmlhint'

import Meta from 'vue-meta'

// 自动扫描组件
import './components/index.js'

// Axios封装类
import HttpPlugin from './utils/HttpPlugin'

// 全局工具类
import PnUtil from './utils/PnUtil'
import PnDict from './utils/PnDict'
import PnChartDict from './utils/PnChartDict'
import PnDesigner from './utils/PnDesigner'
import PnCodeInsight from './utils/PnCodeInsight'
import DesignerCommonUtil from './utils/DesignerCommonUtil'
import ReactiveDesignerCommonUtil from './utils/ReactiveDesignerCommonUtil'
import FormDesignerCommonUtil from './utils/FormDesignerCommonUtil'
import EventBus from './utils/EventBus'

import PnApi from './api/PnApi'
import WebSocketUtil from "./utils/WebSocketUtil";

import axios from 'axios'

// 引入全局样式表
import './assets/css/pnStyle.css'

// 引入自定义图标库
import './assets/iconfont/iconfont.css'
import './assets/customIconfont/iconfont.css'

// 引入自定义指令
import './directives/directives'

Vue.use(iView, {
  transfer: true
});
Vue.use(HttpPlugin);
Vue.use(AsyncComputed);
Vue.use(Viewer);
Vue.use(Meta);
Vue.use(VueCodemirror)

Vue.config.productionTip = false;

Vue.component('vue-ueditor-wrap', VueUeditorWrap);

// 注册Vue全局变量
Vue.prototype.$PnUtil = PnUtil;
Vue.prototype.$PnDict = PnDict;
Vue.prototype.$PnChartDict = PnChartDict;
Vue.prototype.$PnDesigner = PnDesigner;
Vue.prototype.$PnCodeInsight = PnCodeInsight;
Vue.prototype.$DesignerCommonUtil = DesignerCommonUtil;
Vue.prototype.$ReactiveDesignerCommonUtil = ReactiveDesignerCommonUtil;
Vue.prototype.$FormDesignerCommonUtil = FormDesignerCommonUtil;
Vue.prototype.$EventBus = EventBus;
Vue.prototype.$PnApi = PnApi;
Vue.prototype.$WebSocketUtil = WebSocketUtil;
Vue.prototype.$Echarts = echarts;
Vue.prototype.$ = $;

// 注册全局变量
window.PnUtil = PnUtil;
window.EventBus = EventBus;
window.PnApi = PnApi;
window.Echarts = echarts;
window.$ = $;
window.BASE_PATH = PnUtil.getRootPath();
window.axios = axios

// 注册Codemirror Lint全局变量
window.JSHINT = jshint.JSHINT
window.HTMLHint = HTMLHint

// 给数组注册去重插入函数
Array.prototype.pushNoRepeat = function () {
  for(let i=0; i < arguments.length; i++) {
    let ele = arguments[i];
    if(this.indexOf(ele) == -1){
      this.push(ele);
    }
  }
};

// 给字符串注册replaceAll函数
String.prototype.replaceAll = function (str1, str2) {
  return this.replace(new RegExp(str1, 'gm'), str2)
};

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
