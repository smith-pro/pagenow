import {Axios} from '../utils/AxiosPlugin'

const savePnResourceGroup = async function (pnResource) {
  return await Axios.post('/resource/savePnResourceGroup', pnResource);
};

const deletePnResourceGroup = async function (id) {
  return await Axios.delete('/resource/deletePnResourceGroup', {params: {id: id}});
};

const getMyAllPnResourceGroup = async function () {
  return await Axios.get('/resource/getMyAllPnResourceGroup');
};

const getPnResourceGroupById = async function (id) {
  return await Axios.get('/resource/getPnResourceGroupById', {params: {id: id}});
};

const getMyPnResourceByPage = async function (pageIndex, pageSize, group_id) {
  return await Axios.post('/resource/getMyPnResourceByPage', {pageIndex: pageIndex, pageSize: pageSize, group_id: group_id});
};

const deletePnResource = async function (id) {
  return await Axios.delete('/resource/deletePnResource', {params: {id: id}});
};

const resourceTransferToPage = async function (resourceId, pageId) {
  return await Axios.post('/resource/resourceTransferToPage', {resourceId: resourceId, pageId: pageId});
};

export default {
  savePnResourceGroup,
  deletePnResourceGroup,
  getMyAllPnResourceGroup,
  getPnResourceGroupById,
  getMyPnResourceByPage,
  deletePnResource,
  resourceTransferToPage
}
