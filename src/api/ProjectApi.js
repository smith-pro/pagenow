import {Axios} from '../utils/AxiosPlugin'

const saveProject = async function (project) {
  return await Axios.post('/project/saveProject', project);
};

const getProjectByPage = async function (pageIndex, pageSize, name = '') {
  return await Axios.post('/project/getProjectByPage', {pageIndex: pageIndex, pageSize: pageSize, name: name})
};

const getMyProjects = async function () {
  return await Axios.get('/project/getMyProjects');
}

const deleteProject = async function (id) {
  return await Axios.delete('/project/deleteProject', {params: {id: id}});
};

const getProjectById = async function (id) {
  return await Axios.get('/project/getProjectById', {params: {id: id}});
};

const getProjectDevelopers = async function (project_id) {
  return await Axios.get('/project/getProjectDevelopers', {params: {project_id: project_id}});
}

const addDeveloper = async function (project_id, user_ids) {
  return await Axios.post('/project/addDeveloper', {project_id: project_id, user_ids: user_ids});
}

const relieveDeveloper = async function (project_id, user_id) {
  return await Axios.post('/project/relieveDeveloper', {project_id: project_id, user_id: user_id})
}

export default {
  saveProject,
  getProjectByPage,
  getMyProjects,
  deleteProject,
  getProjectById,
  getProjectDevelopers,
  relieveDeveloper,
  addDeveloper
}
