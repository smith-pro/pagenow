import {Axios} from '../utils/AxiosPlugin'

const screenShot = async function (filename, pageId, pagePublishUrl, winWidth, winHeight, developCanvas) {
  return await Axios.post('/file/screenShotPage',
    {filename: filename, pageId: pageId, pagePublishUrl: pagePublishUrl, winWidth: winWidth, winHeight: winHeight, developCanvas: developCanvas});
};

export default {
  screenShot
}
