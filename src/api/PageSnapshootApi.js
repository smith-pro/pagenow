import {Axios} from '../utils/AxiosPlugin'

const savePageSnapshoot = async function (pageSnapshoot) {
  return await Axios.post('/pageSnapshoot/savePageSnapshoot', pageSnapshoot);
};

const getPageSnapshootsByPageId = async function (pageId) {
  return await Axios.get('/pageSnapshoot/getPageSnapshootsByPageId', {params: {pageId: pageId}});
};

const getPageSnapshootByEnabled = async function (pageId, enabled) {
  return await Axios.get('/pageSnapshoot/getPageSnapshootByEnabled', {params: {pageId: pageId, enabled: enabled}});
};

const getPageSnapshootById = async function (id) {
  return await Axios.get('/pageSnapshoot/getPageSnapshootById', {params: {id: id}});
};

const setPageSnapshootEnabled = async function (id, enabled) {
  return await Axios.post('/pageSnapshoot/setPageSnapshootEnabled', {id: id, enabled: enabled});
};

const setPageSnapshootEnabledByPageId = async function (pageId, enabled) {
  return await Axios.post('/pageSnapshoot/setPageSnapshootEnabledByPageId', {pageId: pageId, enabled: enabled});
};

const deletePageSnapshoot = async function (ids) {
  return await Axios.delete('/pageSnapshoot/deletePageSnapshoot', {params: {ids: ids.toString()}});
};

export default {
  savePageSnapshoot,
  getPageSnapshootsByPageId,
  getPageSnapshootByEnabled,
  getPageSnapshootById,
  setPageSnapshootEnabled,
  setPageSnapshootEnabledByPageId,
  deletePageSnapshoot
}
