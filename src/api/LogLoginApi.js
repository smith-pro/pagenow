import {Axios} from '../utils/AxiosPlugin'

const getLogLoginByPage = async function (pageIndex, pageSize, username = '', login_time = '') {
  return await Axios.post('/logLogin/getLogLoginByPage', {pageIndex: pageIndex, pageSize: pageSize, username: username, login_time: login_time});
};

const clearLogLogin = async function () {
  return await Axios.post('/logLogin/clearLogLogin');
}

export default {
  getLogLoginByPage,
  clearLogLogin
}
