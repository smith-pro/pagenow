import {Axios} from '../utils/AxiosPlugin'

const registerNewUser = async function (registerNewUserForm) {
  return await Axios.post('/user/registerNewUser', registerNewUserForm);
};

const saveUser = async function (user) {
  return await Axios.post('/user/saveUser', user);
};

const getUserByPage = async function (pageIndex, pageSize, username = '') {
  return await Axios.post('/user/getUserByPage', {pageIndex: pageIndex, pageSize: pageSize, username: username});
};

const getUserById = async function (id) {
  return await Axios.get('/user/getUserById', {params: {id: id}})
}

const deleteUser = async function (id) {
  return await Axios.delete('/user/deleteUser', {params: {id: id}});
};

const changePassword = async function (changePasswordForm) {
  return await Axios.post('/user/changePassword', changePasswordForm);
};

const resetPassword = async function (userId) {
  return await Axios.post('/user/resetPassword', {userId: userId});
};

const findUsersByUsername = async function (username) {
  return await Axios.get('/user/findUsersByUsername', {params: {username: username}})
}

export default {
  registerNewUser,
  saveUser,
  getUserByPage,
  getUserById,
  deleteUser,
  changePassword,
  resetPassword,
  findUsersByUsername
}
