import {Axios} from '../utils/AxiosPlugin'

const getProjectTotalCount = async function () {
  return await Axios.get('/statistics/getProjectTotalCount');
};

const getPageTotalCount = async function () {
  return await Axios.get('/statistics/getPageTotalCount');
};

export default {
  getProjectTotalCount,
  getPageTotalCount
}
