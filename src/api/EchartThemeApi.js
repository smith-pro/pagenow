import {Axios} from '../utils/AxiosPlugin'

const saveEchartTheme = async function (echartTheme) {
  return await Axios.post('/echartTheme/saveEchartTheme', echartTheme);
};

const getAllEchartTheme = async function () {
  return await Axios.get('/echartTheme/getAllEchartTheme');
};

const getEchartThemeByPage = async function (pageIndex, pageSize) {
  return await Axios.post('/echartTheme/getEchartThemeByPage', {pageIndex: pageIndex, pageSize: pageSize});
};

const getEchartThemeById = async function (id) {
  return await Axios.get('/echartTheme/getEchartThemeById', {params: {id: id}});
};

const deleteEchartTheme = async function (id) {
  return await Axios.delete('/echartTheme/deleteEchartTheme', {params: {id: id}});
};

const getSysEchartThemes = async function () {
  return await Axios.get('/echartTheme/getSysEchartThemes');
};

const getMyEchartThemes = async function () {
  return await Axios.get('/echartTheme/getMyEchartThemes');
};

export default {
  saveEchartTheme,
  getAllEchartTheme,
  getEchartThemeByPage,
  getEchartThemeById,
  deleteEchartTheme,
  getSysEchartThemes,
  getMyEchartThemes
}
