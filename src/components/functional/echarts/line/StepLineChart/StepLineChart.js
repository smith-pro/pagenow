const buildSeriesObj = function (name = '') {
  return {
    name: name,
    type: 'line',
    step: 'start',
    label: {
      show: false,
      position: 'inside',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      borderColor: '#fff',
      borderWidth: 0,
      borderType: 'solid',
      shadowBlur: 8,
      shadowColor: ''
    },
    lineStyle: {
      type: 'solid',
      width: 2
    },
    areaStyle: {
      show: false,
      normal: {
        startColor: '',
        endColor: '',
        shadowColor: '#1565c0',
        shadowBlur: 20
      }
    },
    smooth: false, //是否平滑曲线显示
    symbol: "circle", //标记的图形为实心圆
    symbolSize: 0
  }
};

const buildDefaultSeriesObj = function (name = '', data) {
  return {
    name: name,
    type: 'line',
    step: 'start',
    label: {
      show: false,
      position: 'inside',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      borderColor: '#fff',
      borderWidth: 0,
      borderType: 'solid',
      shadowBlur: 8,
      shadowColor: ''
    },
    lineStyle: {
      type: 'solid',
      width: 2
    },
    areaStyle: {
      show: true,
      normal: {
        startColor: '',
        endColor: '',
        shadowColor: '#1565c0',
        shadowBlur: 20
      }
    },
    smooth: false, //是否平滑曲线显示
    symbol: "circle", //标记的图形为实心圆
    symbolSize: 0,
    data: data
  }
}

export default {
  buildSeriesObj,
  buildDefaultSeriesObj
}
