import PnDesigner from "@/utils/PnDesigner";

const buildSeriesObj = function (name = '') {
  return {
    name: name,
    type: 'scatter',
    symbol: 'circle',
    symbolSize: 20,
    symbolSizeCallbackFunctionCode: '(data)=>{\n  return data[1]\n}',
    symbolRotate: 0,
    label: {
      show: false,
      position: 'inside',
      distance: 5,
      rotate: 0,
      formatter: "(params) => {\n  return params.value\n}",
      color: '#fff',
      fontSize: 12
    },
    labelLine: {
      show: false
    },
    itemStyle: {
      color: '',
      borderColor: "",
      borderWidth: 0,
      borderType: 'solid',
      shadowColor: "",
      shadowBlur: 0,
      opacity: 0.8
    },
    markLine: PnDesigner.buildMarkLineConfigData(),
    markPoint: PnDesigner.buildMarkPointConfigData()
  }
};

const buildDefaultSeriesObj = function (name = '', data) {
  return {
    name: name,
    type: 'scatter',
    symbol: 'circle',
    symbolSize: 20,
    symbolRotate: 0,
    label: {
      show: false,
      position: 'inside',
      distance: 5,
      rotate: 0,
      formatter: "(params) => {\n  return params.value\n}",
      color: '#fff',
      fontSize: 12
    },
    labelLine: {
      show: false
    },
    itemStyle: {
      color: '',
      borderColor: "",
      borderWidth: 0,
      borderType: 'solid',
      shadowColor: "",
      shadowBlur: 0,
      opacity: 0.8
    },
    markLine: PnDesigner.buildMarkLineConfigData(),
    markPoint: PnDesigner.buildMarkPointConfigData(),
    data: data
  }
}

export default {
  buildSeriesObj,
  buildDefaultSeriesObj
}
