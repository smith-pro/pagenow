const buildSeriesObj = function (name = '', stack = '') {
  return {
    name: name,
    type: 'bar',
    stack: stack,
    showBackground: false,
    backgroundStyle: {
      color: 'rgba(180, 180, 180, 0.2)',
      borderColor: '#424242',
      borderWidth: 0,
      borderType: 'solid',
      borderRadius: 0,
      shadowBlur: 0,
      shadowColor: '#424242'
    },
    label: {
      show: false,
      position: 'inside',
      rotate: 0,
      formatter: '{c}',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      startColor: '',
      endColor: '',
      borderColor: '',
      borderWidth: 0,
      borderType: 'solid',
      barBorderRadius: 0,
      shadowBlur: 0,
      shadowColor: '#424242'
    },
    barWidth: '',
    barGap: '20%', // 不同系列的柱间距离
    barCategoryGap: '20%', // 同一系列的柱间距离
  }
};

const buildDefaultSeriesObj = function (name = '', data) {
  return {
    name: name,
    type: 'bar',
    stack: '',
    showBackground: false,
    backgroundStyle: {
      color: 'rgba(180, 180, 180, 0.2)',
      borderColor: '#424242',
      borderWidth: 0,
      borderType: 'solid',
      borderRadius: 0,
      shadowBlur: 0,
      shadowColor: '#424242'
    },
    label: {
      show: false,
      position: 'inside',
      rotate: 0,
      formatter: '{c}',
      color: '#fff',
      fontSize: 12
    },
    itemStyle: {
      color: '',
      startColor: '',
      endColor: '',
      borderColor: '',
      borderWidth: 0,
      borderType: 'solid',
      barBorderRadius: 0,
      shadowBlur: 0,
      shadowColor: '#424242'
    },
    barWidth: '',
    barGap: '20%', // 不同系列的柱间距离
    barCategoryGap: '20%', // 同一系列的柱间距离
    data: data
  }
}

export default {
  buildSeriesObj,
  buildDefaultSeriesObj
}
