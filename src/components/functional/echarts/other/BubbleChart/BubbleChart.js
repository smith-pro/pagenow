const buildSeriesObj = function (color = '') {
  return {
    symbol: 'circle',
    symbolImage: '',

    itemStyle: {
      color: color,
      borderWidth: 0,
      borderColor: '',
      shadowBlur: 0,
      shadowColor: ''
    }
  }
};

export default {
  buildSeriesObj
}
